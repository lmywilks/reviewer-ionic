import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoginService } from './login.service';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private login : FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private Auth: LoginService,
    private appService: AppService,
    private router: Router
  ) {
    this.login = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  logForm() {
    this.Auth.Login(this.login.value)
      .subscribe((res: any) => {
        this.appService.set_config = res;
        this.router.navigate(['home/todo']);
      });
  }
}
