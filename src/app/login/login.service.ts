import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class LoginService {
    readonly API_ROOT = environment.API_ROOT;

    constructor(
        private httpClient: HttpClient
    ) { }

    Login(login: any) {
        return this.httpClient.post(`${ this.API_ROOT }login`, login);
    }

    Signup(signup: any) {
        return this.httpClient.post(`${ this.API_ROOT }signup`, signup);
    }
}
