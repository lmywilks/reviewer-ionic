import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../todo/todo.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.page.html',
  styleUrls: ['./todo-detail.page.scss'],
})
export class TodoDetailPage implements OnInit {

  todo: any = {};

  constructor(
    private activeRoute: ActivatedRoute,
    private todoService: TodoService,
    private router: Router,
    private alertController: AlertController,
    private zone: NgZone
  ) {
    this.activeRoute.params.subscribe(params => {
      if (params && params.id && params.id === 'add') {
        this.todo.name = '',
        this.todo.date = new Date().toISOString();
        this.todo.list = [];
      } else {
        this.todoService.Retrieve(params.id)
          .subscribe((res: any) => {
            this.todo = res;
          });
      }
    });
  }

  ngOnInit() {
  }

  async add_item() {
    const alert = await this.alertController.create({
      header: 'Add new item',
      inputs: [
        {
          name: 'title',
          type: 'text',
          placeholder: 'Enter item name.'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Add',
          handler: (value) => {
            if (this.todo._id) {
              this.todoService.CreateItem(
                this.todo._id,
                { title: value.title }
              )
                .subscribe(res => {
                  this.todo.list.push(res);
                });
            } else {
              this.todo.list.push({
                title: value.title
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async remove_item(item, index, event) {
    event.preventDefault();
    event.stopPropagation();

    const alert = await this.alertController.create({
      header: 'Remove',
      message: `Are you sure you want to remove ${ item.title }.`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Remove',
          cssClass: 'danger',
          handler: () => {
            if (item._id) {
              this.todoService.RemoveItem(this.todo._id, item._id)
                .subscribe(res => {
                  this.todo.list.splice(index, 1);
                });
            } else {
              this.todo.list.splice(index, 1);
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async edit_item(item, index, event) {
    event.preventDefault();
    event.stopPropagation();

    const alert = await this.alertController.create({
      header: 'Edit',
      inputs: [
        {
          name: 'title',
          type: 'text',
          value: item.title,
          placeholder: 'Enter item name.'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Save',
          handler: (value) => {
            item.title = value.title;
            this.todoService.SaveItem(this.todo._id, item)
              .subscribe();
          }
        }
      ]
    });

    await alert.present();
  }

  change_status(item, index, event) {
    event.preventDefault();
    event.stopPropagation();
    setTimeout(() => {
      if (item.status === 'open') {
        item.status = 'done';
      } else {
        item.status = 'open';
      }

      this.todoService.SaveItem(this.todo._id, item)
        .subscribe();
    });
  }

  save() {
    if (this.todo._id) {
      this.todoService.Save(this.todo)
        .subscribe(res => {
          this.router.navigate(['home/todo']);
        });
    } else {
      this.todoService.Create(this.todo)
        .subscribe(res => {
          this.router.navigate(['home/todo']);
        });
    }
  }

  async delete() {
    const alert = await this.alertController.create({
      header: 'Delete',
      message: `Are you sure you want to delete ${ this.todo.name }.`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'delete',
          cssClass: 'secondary',
          handler: () => {
            this.todoService.Remove(this.todo._id)
              .subscribe(() => {
                this.zone.run(() => {
                  this.router.navigate(['home/todo']);
                });
              });
          }
        }
      ]
    });

    await alert.present();
  }
}
