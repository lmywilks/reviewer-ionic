import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  signup: FormGroup;

  constructor(
    private router: Router,
    private login: LoginService,
    private formBuilder: FormBuilder,
    private appService: AppService
  ) {
    this.signup = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      first_name: [''],
      last_name: [''],
      address: [''],
      phone: [''],
      email: ['', Validators.email],
      website: ['']
    });
  }

  ngOnInit() {
  }

  create() {
    console.log(this.signup.value);
    return this.login.Signup(this.signup.value)
      .subscribe(res => {
        this.appService.set_config = res;
        this.router.navigate(['home/todo']);
      });
  }

}
