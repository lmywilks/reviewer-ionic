import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingPage } from './accounting.page';

describe('AccountingPage', () => {
  let component: AccountingPage;
  let fixture: ComponentFixture<AccountingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
