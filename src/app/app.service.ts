import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({providedIn: 'root'})
export class AppService {

    private TOKEN: any;
    private USER_ID: any;

    constructor(private storage: Storage) { }

    init() : Promise<any> {
        return new Promise((resolve, reject) => {
            this.storage.get('token').then(res => {
                if (res) {
                    const token = JSON.parse(res);
                    this.set_token = token.token;
                    this.set_user_id = token.id;
                }
                resolve();
            });
        });
    }

    get user_id() {
        return this.USER_ID;
    }

    set set_user_id(id) {
        this.USER_ID = id;
    }

    get get_token() {
        return this.TOKEN;
    }

    set set_token(token) {
        this.TOKEN = token;
    }

    set set_config(res) {
        this.set_token = res.token;
        this.set_user_id = res.id;
        this.storage.set('token', JSON.stringify(res));
    }
}
