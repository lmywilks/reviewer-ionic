import { Component, OnInit, Input } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  @Input() value: number;

  menu_list = [
    {
      icon: 'document',
      title: 'Profile',
      url: ''
    },
    {
      icon: 'settings',
      title: 'Setting',
      url: ''
    },
    {
      icon: 'log-out',
      title: 'Log Out',
      url: 'login'
    }
  ];

  constructor(
    public navParams: NavParams,
    private router: Router,
    private login: LoginService,
    private storage: Storage
  ) {
  }

  ngOnInit() {}

  dismiss_modal() {
    this.navParams.data.modal.dismiss();
  }

  redirect(url: string) {
    this.dismiss_modal();
    if (url === 'login') {
      this.storage.remove('token').then(() => { this.router.navigate([url]); });
    } else {
      this.router.navigate([url]);
    }
  }
}
