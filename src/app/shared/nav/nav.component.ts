import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MenuComponent } from '../menu/menu.component';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {

  @Input() title: string;
  @Input() default_href: string;
  @Input() back = false;

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  async present_menu() {
    const menu = await this.modalController.create({
      component: MenuComponent,
      componentProps: { value: 123 }
    });
    return await menu.present();
  }

}
