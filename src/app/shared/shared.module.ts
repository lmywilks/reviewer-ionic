import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NavComponent } from './nav/nav.component';
import { MenuComponent } from './menu/menu.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
    ],
    exports: [
        NavComponent,
        MenuComponent
    ],
    declarations: [
        NavComponent,
        MenuComponent
    ],
    entryComponents: [
        MenuComponent
    ],
    providers: [],
})
export class SharedModule { }
