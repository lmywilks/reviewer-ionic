import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { AppService } from './app.service';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

    private TOKEN : any;

    constructor(
        private router     : Router,
        private appService : AppService
    ) {
        this.TOKEN = this.appService.get_token;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.TOKEN) {
            return true;
        } else {
            this.router.navigate(['login']);
            return false;
        }
    }
}
