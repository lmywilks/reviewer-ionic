import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'home',
    component: TabsPage,
    children: [
      {
        path: 'todo',
        children: [
          {
            path: '',
            loadChildren: '../todo/todo.module#TodoPageModule'
          }
        ]
      },
      {
        path: 'restaurant',
        children: [
          {
            path: '',
            loadChildren: '../restaurant/restaurant.module#RestaurantPageModule'
          }
        ]
      },
      {
        path: 'accounting',
        children: [
          {
            path: '',
            loadChildren: '../accounting/accounting.module#AccountingPageModule'
          }
        ]
      },
      {
        path: 'review',
        children: [
          {
            path: '',
            loadChildren: '../review/review.module#ReviewPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/home/todo',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home/todo',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
