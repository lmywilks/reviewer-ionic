import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AppService } from '../app.service';

@Injectable({providedIn: 'root'})
export class TodoService {
  readonly API_ROOT = environment.API_ROOT;
  private user_id: any;

  constructor(
    private httpClient: HttpClient,
    private appService: AppService
  ) {
    this.user_id = this.appService.user_id;
  }

  List() {
    return this.httpClient.get(`${ this.API_ROOT }user/${ this.user_id }/todos`);
  }

  Retrieve(id) {
    return this.httpClient.get(`${ this.API_ROOT }user/${ this.user_id }/todo/${ id }`);
  }

  ListItem(id) {
    return this.httpClient.get(`${ this.API_ROOT }user/${ this.user_id }/todo/${ id }/items`);
  }

  RemoveItem(todo_id, id) {
    return this.httpClient.delete(`${ this.API_ROOT }user/${ this.user_id }/todo/${ todo_id }/item/${ id }`);
  }

  Remove(id) {
    return this.httpClient.delete(`${ this.API_ROOT }user/${ this.user_id }/todo/${ id }`);
  }

  Save(todo) {
    return this.httpClient.put(`${ this.API_ROOT }user/${ this.user_id }/todo/${ todo._id }`, todo);
  }

  SaveItem(todo_id, item) {
    return this.httpClient.put(`${ this.API_ROOT }user/${ this.user_id }/todo/${ todo_id }/item/${ item._id }`, item);
  }

  Create(todo) {
    return this.httpClient.post(`${ this.API_ROOT }user/${ this.user_id }/todo`, todo);
  }

  CreateItem(todo_id, item) {
    if (!item.todo) {
      item.todo = todo_id;
    }

    if (!item.user) {
      item.user = this.user_id;
    }

    return this.httpClient.post(`${ this.API_ROOT }user/${ this.user_id }/todo/${ todo_id }/item`, item);
  }

  Search(query: string) {
    return this.httpClient.get(`${ this.API_ROOT }user/${ this.user_id }/todo/search/${ query }`);
  }
}
