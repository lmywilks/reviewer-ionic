import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.page.html',
  styleUrls: ['./todo.page.scss'],
})
export class TodoPage implements OnInit {

  todos: any[];
  query: string;

  constructor(
    private todoService: TodoService
  ) { }

  ngOnInit() {
    this.refresh();
  }

  refresh(event?: any) {
    this.todoService.List()
      .subscribe((res: any[]) => {
        this.todos = res;
        if (event) {
          event.target.complete();
        }
      });
  }

  change_status(todo, event) {
    event.preventDefault();
    event.stopPropagation();
    setTimeout(() => {
      if (todo.status === 'open') {
        todo.status = 'done';
      } else {
        todo.status = 'open';
      }

      this.todoService.Save(todo)
        .subscribe();
    });
  }

  search() {
    if (this.query) {
      this.todoService.Search(this.query)
        .subscribe((res: any[]) => {
          this.todos = res;
        });
    } else {
      this.refresh();
    }
  }

}
