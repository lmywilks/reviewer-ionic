
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppService } from './app.service';

@Injectable({providedIn: 'root'})
export class HeaderInterceptor implements HttpInterceptor {
    private TOKEN: any;

    constructor(private appService: AppService) {
        this.TOKEN = this.appService.get_token;
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const headers = req.headers
            .set('lmywilks-token', this.TOKEN || false);
        const authReq = req.clone({ headers });
        return next.handle(authReq);
    }
}
